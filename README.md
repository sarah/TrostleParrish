# Trostle-Parrish: A somewhat homomorphic cryptosystem


TrostleParrish implements a somewhat-homomorphic cryptosystem outlined by [1] and [2]. Designed for efficient encryption in a PIR scheme.

Note that Trostle-Parrish has been broken [3] this library is for educational purposes only.

* [1] Trostle, Jonathan, and Andy Parrish. "Efficient computationally private information retrieval from anonymity or trapdoor groups." International Conference on Information Security. Springer, Berlin, Heidelberg, 2010.
* [2] Mayberry, Travis, Erik-Oliver Blass, and Agnes Hui Chan. "PIRMAP: Efficient private information retrieval for MapReduce." International Conference on Financial Cryptography and Data Security. Springer, Berlin, Heidelberg, 2013.
* [3] Lepoint, Tancrède, and Mehdi Tibouchi. "Cryptanalysis of a (somewhat) additively homomorphic encryption scheme used in pir." International Conference on Financial Cryptography and Data Security. Springer, Berlin, Heidelberg, 2015.


