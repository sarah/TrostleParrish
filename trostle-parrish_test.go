package trostle_parrish

import (
	"math/big"
	"testing"
)

func TestEncryptDecrypt(t *testing.T) {
	// These are representative encryption params
	pk := Generate(30, 255)
	plaintext := big.NewInt(14)
	ciphertext := pk.Encrypt(plaintext)
	t.Logf("Encrypted: %v", ciphertext)
	decrypted := pk.Decrypt(ciphertext)
	t.Logf("Decrypted: %v", decrypted)
	if decrypted.Cmp(plaintext) != 0 {
		t.Error("Failed to Decrypt")
	}
}

func TestMultipleAdditions(t *testing.T) {
	// These params are ridiculous low, but they enable us to
	// check that addition is working as expected.
	pk := Generate(6, 32)
	plaintext := big.NewInt(1)
	total := pk.Encrypt(plaintext)
	for i := 0; i < 128; i++ {
		plaintext := big.NewInt(5)
		ciphertext := pk.Encrypt(plaintext)
		total = Add(total, ciphertext)
		t.Logf("Encrypted: %v", total)
		decrypted := pk.Decrypt(total)
		t.Logf("Decrypted: %v", decrypted)
		//  We should see a wrap around at 16 based on our encryption params
		if decrypted.Cmp(big.NewInt(int64(((i+1)*5)+1))) != 0 {
			t.Errorf("Should have been able to do ~2^6 additions = 64, instead: %v", i)
		}
	}

}
